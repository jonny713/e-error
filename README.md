# Описание
* Расширяет стандартный класс Error, добавляя возможность использовать в качестве сообщения объект

# Подключение
Подключаем модуль к проекту
```
npm i @vl-utils/e-error
```
Объявляем для дальнейщего использования
```
const eError = require('@vl-tests/e-error')
```

# Использование
У созданного объекта класса eError имеется дополнительное поле parsedMessage, которое содержит оригинальный объект
```
try {
  throw new eError({
    errorCode: 505,
    errorDescription: 'Oops'
  })
} catch (e) {
  // e.parsedMessage === {
  //   errorCode: 505,
  //   errorDescription: 'Oops'
  // }
}
```
Сохраняется возможность стандартного использования 
```
try {
  throw new eError('Oops')
} catch (e) {
  // e.parsedMessage === 'Oops'
}
```
