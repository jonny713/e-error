class ExtendedError extends Error {
  constructor(message) {
    let parsedMessage = message
    if (typeof message == 'object') {
      message = JSON.stringify(message)
    }
    super(message)

    this.parsedMessage = parsedMessage
    return this
  }
}

module.exports = ExtendedError
