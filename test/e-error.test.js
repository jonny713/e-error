const eError = require('../index')
const assert = require('assert')

describe('extended error', () =>{
  it('throw with string', () => {
    try {
      throw new eError('Oops')
    } catch (e) {
      assert.equal(typeof e.message, "string")
      assert.equal(typeof e.parsedMessage, "string")
    }
  })

  it('throw with object', () => {
    try {
      throw new eError({
        errorCode: 505,
        errorDescription: 'Oops'
      })
    } catch (e) {
      assert.equal(typeof e.message, "string")
      assert.equal(typeof e.parsedMessage, "object")
    }
  })
})
